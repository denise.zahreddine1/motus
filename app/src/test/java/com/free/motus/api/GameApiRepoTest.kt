package com.free.motus.api


import assertk.assertThat
import assertk.assertions.isInstanceOf
import com.free.motus.api.repository.GameApiRepo
import com.free.motus.data.MockData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class GameApiRepoTest {
    @Mock
    private lateinit var retrofitInstance: RestApi

    @Before
    fun setup() {
    }

    @Test
    fun retrieveListOfWordsAPI() = runTest {
        // GIVEN
        whenever(retrofitInstance.getListOfWords()).doReturn(MockData.MOCK_LIST_WORDS)

        // WHEN
        val albumsApi = GameApiRepo(retrofitInstance)
        val result = albumsApi.getWordsApiRepo()

        // THEN
        result.body()?.let {
            assertThat(it).isInstanceOf(String::class.java)
        }

        verify(retrofitInstance, times(1)).getListOfWords()

    }


}