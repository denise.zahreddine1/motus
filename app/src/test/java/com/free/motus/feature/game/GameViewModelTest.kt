package com.free.motus.feature.game

import assertk.assertThat
import assertk.assertions.isGreaterThan
import com.free.motus.api.repository.GameApiRepo
import com.free.motus.data.MockData
import com.free.motus.features.game.GameViewModel
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
class GameViewModelTest {

    @Mock
    private lateinit var gameApiRepo: GameApiRepo

    private lateinit var testDispatcher: TestCoroutineDispatcher

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        testDispatcher = TestCoroutineDispatcher()
        Dispatchers.setMain(testDispatcher)
    }

    @Test
    fun retrieveListOfWordsAPI() = runBlockingTest {
        // GIVEN
        whenever(gameApiRepo.getWordsApiRepo()).thenReturn(MockData.MOCK_LIST_WORDS)

        // WHEN
        val gameViewModel = GameViewModel(gameApiRepo)

        // THEN
        verify(gameApiRepo, times(1)).getWordsApiRepo()
        assertThat(gameViewModel.gameUIState.value.allResults?.size ?: 0).isGreaterThan(0)
    }
}
