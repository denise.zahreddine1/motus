package com.free.motus.data

import retrofit2.Response

class MockData {
    companion object {
        val MOCK_LIST_WORDS: Response<String> = Response.success(
            "Hello\nWord"
        )
    }
}