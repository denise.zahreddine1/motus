package com.free.motus.features.game


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.free.motus.R
import com.free.motus.ui.LoaderWrapper

class GameScreen {

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun FindTheWordScreen(viewModel: GameViewModel) {
        var inputText by rememberSaveable { mutableStateOf("") }
        val uiState by viewModel.gameUIState.collectAsState()

        LoaderWrapper(
            isLoading = uiState.isLoading,
            hasError = uiState.errorMessage != null,
            errorMessage = uiState.errorMessage,
            retryAction = { viewModel.getWordsAPI() },
        ) {

            PopupDialog(uiState = uiState, onDismiss = { viewModel.restartGame() })

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Text(
                    text = stringResource(R.string.find_the_word),
                    style = MaterialTheme.typography.bodySmall,
                    modifier = Modifier.padding(bottom = 16.dp)
                )

                Text(
                    text = uiState.wordToFind?.getOrNull(0).toString(),
                    style = MaterialTheme.typography.titleLarge,
                    modifier = Modifier.padding(bottom = 16.dp)
                )
                TextField(
                    value = inputText,
                    onValueChange = { newText -> inputText = newText },
                    label = { Text(stringResource(R.string.enter_text)) },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(bottom = 16.dp),
                    keyboardOptions = KeyboardOptions.Default.copy(
                        keyboardType = KeyboardType.Text, imeAction = ImeAction.Done
                    )
                )

                Button(
                    onClick = { viewModel.submitUserSuggestion(inputText.trim().uppercase()) },
                ) {
                    Text(stringResource(R.string.submit))
                }
                SuggestionList(uiState.userAttempts, uiState.wordToFind.toString())
            }
        }
    }

    //User List Suggestions
    @Composable
    fun SuggestionList(userAttempts: List<String>, wordToFind: String) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            items(items = userAttempts) { userAttempt ->
                SuggestionWordCard(userAttempt = userAttempt, wordToFind)
            }
        }
    }

    //User Word Suggestions UI
    @Composable
    fun SuggestionWordCard(userAttempt: String, wordToFind: String) {
        val characters = userAttempt.toCharArray()
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(vertical = 4.dp),
            elevation = CardDefaults.cardElevation(4.dp)
        ) {
            LazyRow() {

                items(count = characters.size, itemContent = { index ->
                    if (checkCharacterPosition(index, userAttempt, wordToFind))
                        CircularText(
                            text = characters[index].toString(), Color.Red, Color.Black
                        )
                    else if (checkIfCharacterExist(characters[index], wordToFind)) CircularText(
                        text = characters[index].toString(), Color.Yellow, Color.Black
                    )
                    else CircularText(
                        text = characters[index].toString(), Color.Transparent, Color.Black
                    )


                })
            }
        }
    }

    // UI for Each Character in a Word
    @Composable
    fun CircularText(text: String, backgroundColor: Color, textColor: Color) {
        Box(
            modifier = Modifier
                .padding(8.dp)
                .size(30.dp)
                .background(backgroundColor, shape = CircleShape),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = text,
                textAlign = TextAlign.Center,
                color = textColor,
                style = MaterialTheme.typography.bodySmall,
                modifier = Modifier.padding(8.dp)
            )
        }
    }

    //End Game Popup to Display User Win or Loss
    @Composable
    fun PopupDialog(
        uiState: GameUIState, onDismiss: () -> Unit
    ) {
        val title = if (uiState.gameState == GameState.WIN) {
            stringResource(R.string.win)
        } else {
            stringResource(R.string.you_lose)
        }
        val desc = if (uiState.gameState == GameState.WIN) {
            stringResource(R.string.congrats_you_win_the_game)
        } else {
            stringResource(R.string.hard_luck_you_lose_try_again)
        }
        if (uiState.gameState != GameState.NEW_GAME) {
            AlertDialog(onDismissRequest = onDismiss,
                title = { Text(text = title) },
                text = { Text(text = desc) },
                confirmButton = {},
                dismissButton = {
                    Button(
                        onClick = onDismiss
                    ) {
                        Text(stringResource(R.string.restart))
                    }
                })
        }
    }

    // Check if a Character Exists in a Word
    private fun checkIfCharacterExist(character: Char, wordToFind: String): Boolean {
        return wordToFind.contains(character)
    }

    // Check if a character has the same position in the word to be found and the user's suggestion
    private fun checkCharacterPosition(
        position: Int, userSuggestion: String, wordToFind: String
    ): Boolean {
        return if (userSuggestion.getOrNull(position) == null || wordToFind.getOrNull(position) == null) false
        else userSuggestion[position].equals(wordToFind[position],true)
    }
}