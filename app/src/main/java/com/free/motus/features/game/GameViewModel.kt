package com.free.motus.features.game

import AppParameters
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.free.motus.api.repository.GameApiRepo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import kotlin.random.Random

@HiltViewModel
class GameViewModel @Inject constructor(private val gameApiRepo: GameApiRepo) : ViewModel() {

    private val _gameUIState = MutableStateFlow(GameUIState())
    val gameUIState = _gameUIState.asStateFlow()

    private var job: Job? = null

    init {
        getWordsAPI()
    }

    // Retrieve a List of Words from an API Call and Choose One for the User to Find
    fun getWordsAPI() {


        job = viewModelScope.launch {
            try {
                this@GameViewModel._gameUIState.update {
                    it.copy(
                        isLoading = true, errorMessage = ""
                    )
                }
                val response = withContext(Dispatchers.IO) {
                    gameApiRepo.getWordsApiRepo()
                }


                _gameUIState.update {
                    if (response.isSuccessful) {
                        val apiWords =
                            response.body()?.split('\n')?.filter { it.isNotEmpty() } ?: emptyList()
                        it.copy(
                            isLoading = false,
                            errorMessage = null,
                            allResults = apiWords.toMutableList(),
                            wordToFind = getRandomStringFromList(apiWords)
                        )
                    } else {
                        it.copy(
                            isLoading = false,
                            errorMessage = "Please try again later"
                        )
                    }
                }

            } catch (e: Exception) {
                this@GameViewModel._gameUIState.update {
                    it.copy(
                        isLoading = false, errorMessage = "Please try again later "
                    )
                }
            }
        }

    }


    // Check the game status and add the word suggestion to a list of suggestions
    fun submitUserSuggestion(userAttempt: String) {
        val newItems: MutableList<String> = mutableListOf<String>().apply {
            addAll(this@GameViewModel._gameUIState.value.userAttempts)
            add(userAttempt)
        }

        if (userAttempt.equals(this._gameUIState.value.wordToFind, true)) {
            this._gameUIState.update {
                it.copy(gameState = GameState.WIN, userAttempts = newItems)
            }
        } else {
            val findWordIndex =
                this._gameUIState.value.allResults?.indexOfFirst { it.equals(userAttempt, true) }
            if (this._gameUIState.value.userAttempts.size > AppParameters.Max_Attemption || findWordIndex != -1
            ) {
                this._gameUIState.update {
                    it.copy(
                        gameState = GameState.LOSE,
                        userAttempts = newItems
                    )
                }
            }else{
                this._gameUIState.update {
                    it.copy(
                        userAttempts = newItems
                    )
                }
            }
        }


    }

    //Get Random String From List
    private fun getRandomStringFromList(strings: List<String>): String {

        val randomIndex = Random.nextInt(strings.size)

        return strings[randomIndex]
    }

    // Restart Game
    fun restartGame() {
        val apiResults = this._gameUIState.value.allResults.orEmpty()
        this._gameUIState.update {
            GameUIState(
                isLoading = false,
                errorMessage = null,
                allResults = apiResults.toMutableList(),
                wordToFind = getRandomStringFromList(apiResults)
            )
        }

    }


    override fun onCleared() {
        job?.cancel()
        super.onCleared()
    }

}

data class GameUIState(
    val wordToFind: String? = null,
    val isLoading: Boolean = false,
    val allResults: MutableList<String>? = null,
    val errorMessage: String? = null,
    val gameState: GameState = GameState.NEW_GAME,
    val userAttempts: MutableList<String> = mutableListOf(),
)

enum class GameState {
    NEW_GAME,
    WIN,
    LOSE
}