package com.free.motus.api.repository


import com.free.motus.api.RestApi
import retrofit2.Response
import javax.inject.Inject

class GameApiRepo @Inject constructor(private val retrofitInstance: RestApi) {
    suspend fun getWordsApiRepo(): Response<String> {
        return retrofitInstance.getListOfWords()
    }
}