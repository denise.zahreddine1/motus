package com.free.motus.api


import retrofit2.Response
import retrofit2.http.GET


interface RestApi {

    @GET("raw/iys4katchh")
    suspend fun getListOfWords(
    ) : Response<String>


}